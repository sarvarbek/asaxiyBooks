﻿using AsaxiyBooks.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AsaxiyBooks.Controllers
{
    [Route("api/book")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly BookDb _dbContext;
        public BookController(BookDb bookDb)
        {
            _dbContext = bookDb; 
        }
        [HttpGet]
        public ActionResult<IEnumerable<Book>> GetBooks()
        {
            return _dbContext.Books;
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<Book>> GetById(int id)
        {
                var book = await _dbContext.Books.FindAsync(id);
                if(book == null) return NotFound();
                return Ok(book);
        }

        [HttpPost]
        public async Task<ActionResult<Book>> Create(Book book)
        {
            await _dbContext.Books.AddAsync(book);
            await _dbContext.SaveChangesAsync();
            return Ok(book);     
        }

        [HttpPut]
        public async Task<ActionResult<Book>> Update(Book book)
        {
            try
            {
                 _dbContext.Books.Update(book);
                 await _dbContext.SaveChangesAsync();
                 return Ok();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return NotFound();
            }
            
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult<Book>> Delete(int id)
        {
                var book = await _dbContext.Books.FindAsync(id);
                if (book == null) return NotFound();
                _dbContext.Books.Remove(book);
                await _dbContext.SaveChangesAsync();
                return Ok();          
        }
    }
}
