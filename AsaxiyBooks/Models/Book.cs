﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AsaxiyBooks.Models
{
    [Table("book")]
    public class Book
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; } 
        [Column("author")]
        public string Author { get; set; } 
        [Column("price")]
        public decimal Price { get; set; }
    }
}
